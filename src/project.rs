
extern crate regex;
extern crate yaml_rust;
use std::io::{self, prelude::*};
use std::path::Path;

macro_rules! input {
    ($a: expr, $b: expr) => {
        crate::project::cli::get_user_input($a, Some($b))
    };
    ($a: expr) => {
        crate::project::cli::get_user_input($a, None)
    };
}

/// Return upm.yml file for current project as Vec<Yaml>
macro_rules! config {
    // This macro should return a hashmap with all the normalized config values
    () => {
        match crate::project::utils::get_config() {
            Ok(v) => v,
            Err(msg) => {
                eprintln!("Error: {}", msg);
                vec![]
            }
        }
    };
}

macro_rules! bump_version {
    ($semverseg: expr) => {
        let v = crate::project::Project::Version::from($vstr);
        v.inc($semverseg)
    };
}

#[doc = "Handles creating the user's project from the name/path they specified."]
pub fn create_project_folder(mut folder_name: &str) -> io::Result<&Path> {
    use std::fs;
    // set project path equal to current directory if the value is empty
    if folder_name.is_empty() {
        folder_name = "./".as_ref();
    }

    let project_path: &Path = Path::new(folder_name);
    // Helper functions
    let gen_config = |path: &Path,
                      version: &String,
                      name: &String,
                      author: &String|
     -> io::Result<()> {
        let mut project_config_file = fs::File::create(&path.join("upm.yml"))?;
        let contents: String = format!(
                    "version  : {version}\nname     : {name}\nauthor   : {author}\ncommands :\n\tstart: echo starting application\n\tls: ls -la\n",
                    version = version,
                    name = name,
                    author = author
                );
        project_config_file.write_all(&contents.as_bytes())?;
        Ok(())
    };
    let prompt_user_create_config = || -> io::Result<()> {
        let mut default_project_name: String = String::new();
        if let Some(last) = Path::new(&folder_name).file_name() {
            if let Some(last_as_str) = last.to_str() {
                default_project_name = String::from(last_as_str);
            } else {
                eprintln!("Error with default project_name");
            };
        } else {
            // eprintln!("Error with default project_name");
            let cwd = std::env::current_dir()?;
            default_project_name = String::from(
                cwd.file_name()
                    .unwrap()
                    .to_str()
                    .expect("Error with default project_name"),
            );
        };
        println!("{}", default_project_name);
        //Get version from user
        let version: String = input!("version", "0.0.1").expect("Trouble getting version");
        //Get project name from user
        let name: String =
            input!("name", default_project_name.as_str()).expect("Trouble getting project name");
        // Get author name from user
        let author: String = input!("author").expect("Trouble getting author name");
        gen_config(&project_path, &version, &name, &author)?;
        std::mem::drop(default_project_name);
        Ok(())
    };
    if project_path.is_relative() || project_path.is_absolute() {
        if project_path.is_dir() {
            let mut counter: i64 = 0;
            for entry in project_path.read_dir().expect("read_dir call failed") {
                if let Ok(_entry) = entry {
                    counter += 1;
                }
            }
            if counter > 0 {
                let mut input = String::new();
                println!("The directory you have specified for initalization already contains other files. What should I do?");
                print!("(O)verwrite directory contents; (A)dd file anyway; (C)ancel: ");
                io::stdout().flush()?;
                loop {
                    io::stdin().read_line(&mut input)?;
                    match input.as_str() {
                        "O" => {
                            // Clear directory contents
                            for entry in fs::read_dir(project_path)? {
                                let entry = entry?;
                                let path = entry.path();
                                if path.is_dir() {
                                    fs::remove_dir_all(path)?;
                                } else {
                                    fs::remove_file(path)?;
                                }
                            }
                            prompt_user_create_config()?;
                            break;
                        }
                        "A" => {
                            // Procede normally
                            prompt_user_create_config()?;
                            break;
                        }
                        "C" => std::process::exit(0),
                        _ => continue,
                    }
                }
                Ok(project_path)
            } else {
                // Safe to create files in dir
                println!("Folder exists but is empty");
                prompt_user_create_config()?;
                Ok(project_path)
            }
        } else {
            // Create dir
            fs::create_dir(&project_path).expect("Could not create dir");
            // create files
            prompt_user_create_config()?;
            Ok(project_path)
        }
    } else {
        return Err(io::Error::from(io::ErrorKind::InvalidInput));
    }
}

pub mod Project {
    // Imports
    use regex::Regex;
    use std::convert::From;
    #[derive(Clone)]
    pub struct V<'a>(u8, u8, u8, &'a str);
    pub type Version<'a> = V<'a>;
    impl<'a> V<'a> {
        pub fn inc(&mut self, svs: &'a str) -> Self {
            match svs {
                "major" => {
                    self.0 += 1;
                }
                "minor" => {
                    self.1 += 1;
                }
                "patch" => {
                    self.2 += 1;
                }
                _ => {
                    eprintln!("Error: Semver segment provided was not valid");
                }
            }
            return self.to_owned();
        }
    }
    impl<'a> Version<'a> {
        fn new() -> Self {
            V(0, 0, 0, "")
        }
        fn major(&mut self, value: u8) -> &Self {
            self.0 = value;
            self
        }
        fn minor(&mut self, value: u8) -> &Self {
            self.1 = value;
            self
        }
        fn patch(&mut self, value: u8) -> &Self {
            self.2 = value;
            self
        }
        fn prerelease(&mut self, value: &'a str) -> &Self {
            self.3 = value;
            self
        }
        fn unwrap(&self) -> Version {
            self.clone()
        }
    }

    // Statically declare regex for versioning
    lazy_static!{
        /**
        * Named capture group descriptions:
        *     - Full match ~ the full version and prerelease string (i.e. 1.2.3-beta.1.2.3)
        *     - full_version ~ the full version string (i.e. 1.2.3)
        *         - major ~ the major segment of the version string (i.e. 1 of 1.2.3)
        *         - minor ~ the minor segment of the version string (i.e. 2 of 1.2.3)
        *         - patch ~ the patch segment of the version string (i.e. 3 of 1.2.3)
        *     - prerelease_and_version ~ the full prerelease and version string (i.e. -beta.1.2.3)
        *         - prerelease ~ a string 1 to 10 characters in length to represent the prerelease label (i.e. beta of -beta.1.2.3)
        *         - version ~ the version segment of the prerelease_and_version string (i.e. 1.2.3 of -beta.1.2.3)
        **/
        static ref VRE: Regex = Regex::new(
            r"^
            (?'full_version'
              (?'major'\d{1,2}) (?:\.)?
              (?'minor'\d{1,2}) (?:\.)?
              (?'patch'\d{1,2})
            )
            (?:
                (?:[-])?
                (?'prerelease_and_version'
                    (?'prerelease'
                      [a-zA-Z]{1,10}
                    )
                    (?:[.])?
                    (?'version'
                        [0-9]{1,2}(?:[.][0-9]{1,2}){0,2}
                    )?
                )
            )?"
            ).expect("Failed to create regex pattern for version information");
    }

    impl<'a> From<&'a str> for Version<'a> {
        fn from(vstring: &'a str) -> Self {
            let cps: regex::Captures<'a> = VRE.captures(vstring).expect("UPM Error: Could not parse capture groups from version string.");
            if VRE.is_match(vstring) {
                V(
                    cps.name("major").unwrap().as_str().as_ptr() as u8,
                    cps.name("minor").unwrap().as_str().as_ptr() as u8,
                    cps.name("patch").unwrap().as_str().as_ptr() as u8,
                    cps.name("prerelease").unwrap().as_str()
                )
            } else {
                Version::new()
            }
        }
    }

    impl<'a> std::fmt::Display for Version<'a> {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            write!(f, "{}.{}.{}{}", self.0, self.1, self.2, self.3)
        }
    }
}

pub mod utils {
    use std::error::Error;
    use std::fs;
    use std::path::Path;
    use yaml_rust::Yaml;
    #[allow(dead_code)]
    pub fn get_config() -> Result<Vec<Yaml>, String> {
        use yaml_rust::YamlLoader;
        let cfg_path: &Path = Path::new("./upm.yml");
        if cfg_path.exists() {
            match fs::read_to_string(cfg_path) {
                Ok(s) => {
                    return Ok(YamlLoader::load_from_str(&s).unwrap());
                }
                Err(_error) => {
                    return Err(String::from(_error.description()));
                }
            };
        } else {
            return Err(String::from("UPM configuration file upm.yml not found."));
        }
    }
}

pub mod cli {
    use std::io::{self, prelude::*};
    use yaml_rust::Yaml;

    pub fn get_user_input(prompt: &str, default: Option<&str>) -> Result<String, io::Error> {
        let mut input: String = String::new();
        // let mut buf = [];
        match default {
            Some(d) => print!("{} ({}): ", prompt, d),
            None => print!("{}: ", prompt),
        }
        io::stdout().flush()?;
        io::stdin()
            .read_line(&mut input)
            .expect("Unable to readline");
        // Get rid of trailiing newline
        input = String::from(input.trim());
        // Set default
        if input.is_empty() {
            input = String::from(default.expect("Empty"));
        }
        Ok(input)
    }

    pub fn exec(command: &str) -> () {
        let docs: Vec<Yaml> = config!();
        let doc = docs[0].clone();
        let cmd_body = doc["commands"][command].as_str().unwrap();
        let mut cli: Vec<&str> = cmd_body.split(' ').collect();
        let mut cmd = std::process::Command::new(cli.remove(0));
        let output = cmd.args(cli).output().expect(
            format!(
                "Uh oh! An error occurred running the {} command. Please see below for details",
                command
            )
            .as_str(),
        );
        println!("status: {}", output.status);
        io::stdout()
            .write_all(&output.stdout)
            .expect("An error occurred at stdout");
        io::stderr()
            .write_all(&output.stderr)
            .expect("An error occurred at stderr");
        // Figure out what is wrong with this code
        /* if let Some(hash) = doc.into_hash() {
            if let Some(commands) = hash[&Yaml::String(String::from("commands"))]
                .clone()
                .into_vec()
            {
                for _item in commands {
                    if let Some(hash) = _item.as_hash() {
                        let command_name: &String = &String::from(command);
                        let mut selected_command: &str = "";
                        if let Some(command_body) = &hash[&Yaml::String(command_name.to_string())].as_str() {
                            selected_command = command_body;
                        } else {
                            eprintln!("spook");
                        }
                        let mut cli: Vec<&str> = selected_command.split(' ').collect();
                        let mut command = std::process::Command::new(cli.remove(0));
                        let output = command
                            .args(cli)
                            .output()
                            .expect(
                                format!("Uh oh! An error occurred running the {} command. Please see below for details", &command_name).as_str());
                        println!("status: {}", output.status);
                        io::stdout().write_all(&output.stdout).expect("An error occurred at stdout");
                        io::stderr().write_all(&output.stderr).expect("An error occurred at stderr");
                    }
                }
            }
        } */
    }

    // Work on custom ErrorKind
}
