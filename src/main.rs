#[macro_use] pub extern crate lazy_static;
#[macro_use]
extern crate clap;
extern crate git2;
extern crate yaml_rust;
#[macro_use]
mod project;
use git2::Repository;
use project::create_project_folder;
use yaml_rust::Yaml;

#[derive(Clap)]
#[clap(author = "Ian Fabs")]
#[allow(dead_code)]
struct Opts {
    /// Any project-defined command; defined in upm.yml
    command: Option<String>,

    #[clap(subcommand)]
    sub: Option<SubCmd>,
}

#[derive(Clap)]
#[allow(non_camel_case_types)]
enum SubCmd {
    /// Initialize a new UPM Project
    init(Init),
    /// Get version information for current project
    version(Version),
}

/// A subcommand for initializing a upm project
#[derive(Clap)]
#[clap(name = "init", version = "1.0")]
#[allow(dead_code)]
struct Init {
    /// The name of the project
    input: String,
    /// Initialize git repo as well
    #[clap(short = "g")]
    git: bool,
}
#[derive(Clap)]
#[clap(name = "version", version = "1.0")]
#[allow(dead_code)]
struct Version {
    // The version segment you want to bump
    r#type: Option<String>,
    #[clap(long = "down")]
    down: bool,
}

fn main() -> Result<(), std::io::Error> {
    let opts: Opts = Opts::parse();

    match opts.sub {
        Some(subcmd) => match subcmd {
            SubCmd::init(i) => {
                if !i.input.is_empty() {
                    let project_path = create_project_folder(i.input.as_ref())?;
                    if i.git {
                        Repository::init(project_path).unwrap();
                    }
                }
            }
            SubCmd::version(v) => {
                let version_string: &str = config!()[0]["version"].as_str().unwrap_or("0.0.0");
                
                let (major, minor, patch) = (
                    String::from("major"),
                    String::from("minor"),
                    String::from("patch"),
                );
                if v.r#type.is_some() {
                    let mut semverseg: &str = "";
                    let t_vtype = v.r#type.clone();
                    if let Some(svstype) = t_vtype {
                        let t = svstype.as_str();
                        let new_version = project::Project::Version::from(version_string).inc(semverseg);
                        config!()[0]["version"].
                    };
                    
                }
            }
        },
        None => {
            // Do Nothing?
            let command = opts.command.unwrap_or(String::from("start"));
            println!("Using command: {}", command);
            project::cli::exec(command.as_str())
        }
    }

    /*
    // Run user commands
    // This block MUST come after every other block
    match matches.value_of("COMMAND") {
        Some(_cmd) => {
            project::cli::exec(_cmd);
        },
        None => eprintln!("Error: No command by that name was found in your project config.")
    } */

    Ok(())
}
